package utfpr.ct.dainf.if62c.avaliacao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(path));
    }
    
    private String getNextLine() throws IOException {
        return reader.readLine();
    }
    
    private Lancamento processaLinha(String linha) {
        if(linha == null)
            return null;
        
        String conta = linha.substring(0, 6);
        String data = linha.substring(6, 14);
        String descricao = linha.substring(14, 74);
        String valor = linha.substring(75, 86);
        
        return new Lancamento(Integer.valueOf(conta), Date.valueOf(data), descricao, Double.valueOf(valor));
    }
    
    private Lancamento getNextLancamento() throws IOException {
        return this.processaLinha(this.getNextLine());
    }
    
    public List<Lancamento> getLancamentos() throws IOException {
        List<Lancamento> lista = new ArrayList<> ();
        
        Lancamento lanc = this.getNextLancamento();
        
        while(lanc != null)
        {
            lista.add(lanc);
            lanc = this.getNextLancamento();
        }
        
        reader.close();
        
        lanc = lista.get(1);
        
        lista.sort((Comparator<? super Lancamento>) new LancamentoComparator(lanc.getConta(),lanc.getData()));
        return lista;
    }
    
}
