package utfpr.ct.dainf.if62c.avaliacao;

import java.util.Date;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class LancamentoComparator implements Comparable<Lancamento> {
    private Integer conta;
    private Date data;
    
    public LancamentoComparator(Integer cont, Date dat){
        conta = cont;
        data = dat;
    }

    @Override
    public int compareTo(Lancamento o) {
        if(this.conta.compareTo(o.getConta()) != 0 ){
            if(this.conta > o.getConta())
                return 1;
            else
                return -1;
        }
        
        if(this.data == o.getData())
            return 0;
        
        if(this.data.after(o.getData()))
            return 1;
        else
            return -1;
            
    }
    
    
}
