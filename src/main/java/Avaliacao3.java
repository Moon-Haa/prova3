
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.avaliacao.Lancamento;
import utfpr.ct.dainf.if62c.avaliacao.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Avaliacao3 {
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
        System.out.println("Favor digitar o nome e o caminho completo do arquivo: ");
        Scanner leitor = new Scanner(System.in);
        ProcessaLancamentos proc_lanc = new ProcessaLancamentos(leitor.nextLine());
        List<Lancamento> lista = new ArrayList<> (proc_lanc.getLancamentos());
        
        Integer conta = 10;
        
        do {
            System.out.println("Digite o numero da conta ou zero para sair: ");
            if(!leitor.hasNextInt())
            {
                System.out.println("Por favor, informe um valor numerico");
                continue;
            }
            
            conta = Integer.parseInt(leitor.nextLine());
                            
            if(conta == 0)
                break;
            
            List<Lancamento> lista2 = new ArrayList<> (exibeLancamentosConta(lista, conta));
            
            if(lista2.isEmpty())
                System.out.println("Conta inexistente.");
            else
            {
                for (Lancamento lista21 : lista2) {
                    System.out.println(lista21.toString());
                }
            }
            
        }
        while (conta != 0);
    }
    
    public static List<Lancamento> exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        int indice = lancamentos.indexOf(conta);
        if(indice < 0)
            return null;
             
        int indice2 = lancamentos.lastIndexOf(conta);
        
        return new ArrayList<> (lancamentos.subList(indice, indice2));
    }
 
}